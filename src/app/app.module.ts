import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NvD3Module } from 'ng2-nvd3';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NavMenuComponent } from './components/nav-menu/nav-menu.component'; 
import { FlexLayoutModule } from  '@angular/flex-layout';
 
import { MaterialModule } from './modules/material.module';
import {
  MatButtonModule, MatDialogModule, MatIconModule, MatInputModule, MatPaginatorModule, MatSortModule,
  MatTableModule, MatToolbarModule, MatFormFieldModule
} from '@angular/material';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component'; 
import { BarchartComponent } from './components/barchart/barchart.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { ChartComponent } from './components/chart/chart.component'; 
import { StockChartD3Component } from './components/stockChart-d3/app.component'; 
import { LineChartComponent } from './components/line-chart/line-chart.component';
import { DonutChartComponent } from './components/donut-chart/donut-chart.component';
import { HighChartComponent } from './components/highchart/line-chart.component';

import { PriceService } from './services/price.service'; 
import { AuthGuardService } from './services/auth-guard.service'; 

// d3 and nvd3 should be included somewhere
import 'd3';
import 'nvd3';


import {routes} from './app.routes';

@NgModule({
  declarations: [
    AppComponent, 
    NavMenuComponent,
    BarchartComponent,
    PieChartComponent,
    HighChartComponent,
    ChartComponent,
    LineChartComponent,
    StockChartD3Component,
    DonutChartComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    BrowserModule,
    FlexLayoutModule,
    NvD3Module,
    HttpClientModule,
    FormsModule,
    MaterialModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ AuthGuardService,  PriceService ,],
  bootstrap: [AppComponent]
})
export class AppModule { }
