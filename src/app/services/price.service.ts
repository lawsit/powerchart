import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';


@Injectable()
export class PriceService {

  urlHost: string = 'https://www.quandl.com/api/v3/datasets/WIKI/';
  ApiKey : string = 'Your API';
  urlFilterParam: string = '/data.json?order=asc&collapse=weekly&start_date=2016-01-01&end_date=2018-04-12&api_key=' + this.ApiKey;
  urlString: string = '';

  constructor(private _http: HttpClient) { }

  getPriceHistory(stockSym) {
    
    console.log('API=' + this.ApiKey);
    this.urlString = this.urlHost + stockSym + this.urlFilterParam;

    return this._http.get(this.urlString)
      .map(result => result);
  }

} 
