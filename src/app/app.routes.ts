import { Routes } from '@angular/router';  
import { LineChartComponent } from './components/line-chart/line-chart.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { DonutChartComponent } from './components/donut-chart/donut-chart.component';
import { ChartComponent } from './components/chart/chart.component'; 
import { BarchartComponent } from './components/barchart/barchart.component'; 
import { HighChartComponent } from './components/highchart/line-chart.component'; 
import { StockChartD3Component } from './components/stockChart-d3/app.component'; 
import { AuthGuardService } from './services/auth-guard.service';

export const routes: Routes = [

    { path: '', component: HighChartComponent, pathMatch: 'full' }, 
    { path: 'line-chart', component: LineChartComponent, canActivate: [AuthGuardService] }, 
    { path: 'stock-chart', component: ChartComponent, canActivate: [AuthGuardService] }, 
    { path: 'high-chart', component: HighChartComponent, canActivate: [AuthGuardService] }, 
    { path: 'pie-chart', component: PieChartComponent, canActivate: [AuthGuardService] },
    { path: 'bar-chart', component: BarchartComponent, canActivate: [AuthGuardService] },
    { path: 'donut-chart', component: DonutChartComponent, canActivate: [AuthGuardService] },
    { path: 'stock-chart-d3', component: StockChartD3Component, canActivate: [AuthGuardService] } 

];