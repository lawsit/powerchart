import { Component, OnInit, ViewEncapsulation   } from '@angular/core';
declare let d3: any;


@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.css'],
   encapsulation: ViewEncapsulation.None
})
export class BarchartComponent implements OnInit {
  tech_title: string = 'Discrete Bar Chart using Angular-nvD3'; 
  title: string = 'Yearly Return by products';


  options;
  data;
  ngOnInit() {
    this.options = {
      chart: {
        type: 'discreteBarChart',
        height: 750,
        margin : {
          top: 20,
          right: 20,
          bottom: 50,
          left: 55
        },
        x: function(d){return d.label;},
        y: function(d){return d.value;},
  

        color : ['#3eb049',  '#649AF6', '#9A97F0', '#C4689E' , '#F7464A', '#97D672'  ],
        showValues: true,
        valueFormat: function(d){
          return d3.format(',.0f')(d) + '%';
        },
        duration: 500,
        xAxis: {
          axisLabel: 'Investment Products'
        },
        yAxis: {
          axisLabel: 'YTD Return (%)',
          axisLabelDistance: -10
        }
      }
    }
    this.data = [
      {
        key: "Cumulative Return",
        values: [
          {
            "label" : "US Stock" ,
            "value" : -20
          } ,
          {
            "label" : "Canada Stock" ,
            "value" : -10
          } ,
          {
            "label" : "Money Market" ,
            "value" : 3
          } ,
          {
            "label" : "Commodity" ,
            "value" : 15
          } ,
          {
            "label" : "Oil" ,
            "value" : 180
          } ,
          {
            "label" : "Bonds" ,
            "value" : 15
          } ,
          {
            "label" : "Real Estate" ,
            "value" : 12
          }  
        ]
      }
    ];
  }



}
