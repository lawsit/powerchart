import { Component, OnInit, ViewEncapsulation   } from '@angular/core';
declare let d3: any;

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class PieChartComponent implements OnInit {
  tech_title: string = 'Pie Chart using Angular-nvD3';
  title: string = 'Invesment Portfolio by sector';
  
  options;
  data;
  ngOnInit() {
    this.options = { 
            chart: {
                type: 'pieChart',
                height: 700,              
                x: function(d){return d.key;},
                y: function(d){return d.y;},
                showLabels: true,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                legend: {
                    margin: {
                        top: 20,
                        right: 50,
                        bottom: 50,
                        left: 30
                    }
                },
                tooltip: {
                         contentGenerator: function (e) {
                           
                          return e.data.key + ' ' + e.data.y;
                          
                        }
                  }
            }
        };


  

    this.data =  [
            {
                key: "Utilities",
                y: 25000
            },
            {
                key: "Oil & Gas",
                y: 10000
            },
            {
                key: "Public Sector",
                y: 5000
            },
            {
                key: "Transportation",
                y: 70000
            },
            {
                key: "Telcom",
                y: 90000
            } 
        ];

        
  }
 
}
