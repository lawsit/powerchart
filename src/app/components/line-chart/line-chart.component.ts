import { Component, OnInit, ViewEncapsulation   } from '@angular/core';
import { PriceService } from '../../services/price.service';

//Reference : http://krispo.github.io/angular-nvd3/#/quickstart

declare let d3: any;


@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  tech_title: string = 'Line Chart using Angular-nvD3'; 
  title: string = 'Price Chart';

  price_date = [];
  stock_price = []; 
  stock_data = []

  

  stockList: any = [
    { id: 0, name: 'IBM' },
    { id: 1, name: 'FB' },
    { id: 2, name: 'MSFT' },
    { id: 3, name: 'TSLA' },
    { id: 4, name: 'C' },
    { id: 5, name: 'JPM' },
    { id: 6, name: 'AMZN' },
    { id: 7, name: 'GS' }
  ];

  current = 2;
  
  options;
  data;
  chartType;

  constructor(private _priceService: PriceService ) {  
  }

  ngOnInit(){
    
    this.subscribeStockData('MSFT'); 

    this.options = {
      chart: {
        type: 'lineChart',
        height: 680,
        margin : {
          top: 50,
          right: 50,
          bottom: 40,
          left: 55
        },
        legend: {
          margin: {
              top: 5,
              right: 100,
              bottom: 5,
              left: 0
          }
        },
        x: function(d){ return d.x; },
        y: function(d){ return d.y; },
        useInteractiveGuideline: true,
        xAxis: {
          axisLabel: 'Date',
          tickFormat: function(d){
            return d3.time.format('%d/%m/%y')(new Date(d));;
          },
        },
        yAxis: {
          axisLabel: 'Stock Price',
          tickFormat: function(d){
            return d3.format('.02f')(d);
          },
          axisLabelDistance: -10
        }
        },
        title: {
          enable: true,
          text: 'Title for Line Chart',
          css: {
            'text-align': 'justify',
            'margin': '80px 83px 80px 87px'
        }
        },
        caption: {
          enable: true,
          html: '<b>Figure 1.</b> Lorem ipsum dolor sit amet, at eam blandit sadipscing, <span style="text-decoration: underline;">vim adhuc sanctus disputando ex</span>, cu usu affert alienum urbanitas. <i>Cum in purto erat, mea ne nominavi persecuti reformidans.</i> Docendi blandit abhorreant ea has, minim tantas alterum pro eu. <span style="color: darkred;">Exerci graeci ad vix, elit tacimates ea duo</span>. Id mel eruditi fuisset. Stet vidit patrioque in pro, eum ex veri verterem abhorreant, id unum oportere intellegam nec<sup>[1, <a href="https://github.com/krispo/angular-nvd3" target="_blank">2</a>, 3]</sup>.',
          css: {
              'text-align': 'justify',
              'margin': '10px 13px 0px 7px'
          }
      }
       
    };
  
    //this.data = this.sinAndCos();
  }
   

  subscribeStockData(sym:String) { 

    this._priceService.getPriceHistory(sym)
      .subscribe(res => {
        console.log('data');

        //console.log(res['dataset_data']);
        var price_date = res['dataset_data'].data.map(getPriceDates)
        var stock_price = res['dataset_data'].data.map(getClosePrices)

        this.stock_data = price_date.map( function(x, i){ return {"x": new Date(x), "y": stock_price[i]} }, this); 
        
        //console.log(this.stock_data); 

        function getPriceDates(item, index) { return item[0]; }
        function getClosePrices(item, index) { return +item[4]; }

        this.data = this.sinAndCos();
        

       })  // End of subscribe
        
  }


  
  sinAndCos() {
    var sin = [],sin2 = [],
      cos = [];
  
    //Data is represented as an array of {x,y} pairs.
    for (var i = 0; i < 100; i++) {
      sin.push({x: i, y: Math.sin(i/10)});
      sin2.push({x: i, y: i % 10 == 5 ? null : Math.sin(i/10) *0.25 + 0.5});
      cos.push({x: i, y: .5 * Math.cos(i/10+ 2) + Math.random() / 10});
    }
  
    console.log(sin);
    //Line chart data should be sent as an array of series objects.
    return [
      
      {
        values: this.stock_data,       
        key: 'MSFT', //key  - the name of the series.
        color: 'orange'  //color - optional: choose your own line color.
      } 
    ];
  }



}
