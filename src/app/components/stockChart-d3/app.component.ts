import { Component, OnInit } from '@angular/core';

import { PriceService } from '../../services/price.service';

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
 

@Component({
  selector: 'linechart-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

 
export class StockChartD3Component implements OnInit {
 
  subtitle: string = 'Stock Chart using d3.js';

  private margin = {top: 20, right: 20, bottom: 30, left: 50};
  private width: number;
  private height: number;
  private x: any;
  private y: any;
  private svg: any;
  private line: d3Shape.Line<[number, number]>;
  
  price_date = [];
  stock_price = []; 
  stock_data = []

  stockList: any = [
    { id: 0, name: 'IBM' },
    { id: 1, name: 'FB' },
    { id: 2, name: 'MSFT' },
    { id: 3, name: 'TSLA' },
    { id: 4, name: 'C' },
    { id: 5, name: 'JPM' },
    { id: 6, name: 'AMZN' },
    { id: 7, name: 'GS' }
  ];

  current = 2; 

  constructor(private _priceService: PriceService ) {  
    this.width = 1600 - this.margin.left - this.margin.right;
    this.height = 680 - this.margin.top - this.margin.bottom;
  }

  ngOnInit() {
    
    this.subscribeStockData(); 
  }
  
  
  logDropdown(id: number): void {
    this.subscribeStockData();
    var svg = d3.select("svg");
    svg.selectAll("*").remove();

  }

  subscribeStockData() {
 
    this._priceService.getPriceHistory(this.stockList[this.current].name)
      .subscribe(res => { 
        this.price_date = res['dataset_data'].data.map(getPriceDates)
        this.stock_price = res['dataset_data'].data.map(getClosePrices)

        this.stock_data = this.price_date.map( function(x, i){ return {"date": new Date(x), "price": this.stock_price[i]} }, this); 
         
        this.initSvg();
        this.initAxis();
        this.drawAxis();
        this.drawLine();

        function getPriceDates(item, index) { return item[0]; }
        function getClosePrices(item, index) { return +item[4]; }
       })   
  }

  private initSvg() {
    this.svg = d3.select("svg")
                 .append("g")
                 .attr('fill', 'blue')
                 .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
  }

  private initAxis() {
    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);

    this.x.domain(d3Array.extent(this.stock_data, (d) => d.date ));
    console.log(this.x.domain);
    this.y.domain(d3Array.extent(this.stock_data, (d) => d.price )); 
  }

  private drawAxis() {

    this.svg.append("g")
          .attr("class", "axis axis--x")
          .attr("transform", "translate(0," + this.height + ")")
          .call(d3Axis.axisBottom(this.x));

    // call the d3.axisLeft method which takes the parameter of y
    this.svg.append("g")
          .attr("class", "axis axis--y")
          .call(d3Axis.axisLeft(this.y))
          .append("text")
          .attr("fill", "#000")
          .attr("class", "axis-title")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", ".71em")
          .style("text-anchor", "end")
          .text("Price ($)");
  }

  private drawLine() {
 
    // Define the div for the tooltip
    var div = d3.select("body").append("div")	
       .attr("class", "tooltip")				
       .style("opacity", 0);
 
   this.line = d3Shape.line()
   //this.line = d3.line()
   .x( (d: any) => this.x(d.date))
   .y( (d: any) => this.y(d.price));
 
    // In the last step, we will be appending a path. This path will actually draw the line chart as per the values of the data.
    // pass our dataset using the datum method and then set the attributes of fill color, stroke color, and width
    this.svg.append("path")
            .datum(this.stock_data)
            .attr("fill", "none")
            .attr("stroke", "steelblue")
            .attr("stroke-linejoin", "round")
            .attr("stroke-linecap", "round")
            .attr("stroke-width", 1.5)
            .attr("class", "line")
            .attr("d", this.line);
  }

}
