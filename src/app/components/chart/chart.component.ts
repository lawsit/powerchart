import { Component } from '@angular/core';
import { PriceService } from '../../services/price.service';
import { Chart } from 'chart.js'; 

 
@Component({
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})


export class ChartComponent {

  subtitle: string = 'Line Chart using chart.js';

  chart: Chart = [];
  price_date = [];
  stock_price = []; 

  stockList: any = [
    { id: 0, name: 'IBM' },
    { id: 1, name: 'FB' },
    { id: 2, name: 'MSFT' },
    { id: 3, name: 'TSLA' },
    { id: 4, name: 'C' },
    { id: 5, name: 'JPM' },
    { id: 6, name: 'AMZN' },
    { id: 7, name: 'GS' }
  ];

  log = '';
  current = 2;
 
  constructor(private _priceService: PriceService ) { }

  ngOnInit() {
    this.subscribeStockData();
  }


  logDropdown(id: number): void {
    this.subscribeStockData();

  }


  createChart() {

    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: this.price_date,
        borderColor: "#0A0AA9",
        datasets: [
          {
            data: this.stock_price,
            //borderColor: '#3cba9f',
            borderColor: '#0D0AA9',
            backgroundColor: "#1ABC9C",
            pointHighlightFill: "#fff",
            fontSize: 16,
            label: "Daily Close",
            fill: false
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        title: {
          display: true,
          fontSize: 25,
          fontColor: '#0D0AA9',

          fontStyle: 'bold',
          text: 'Stock Price Chart - Year 2015 onward'
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }]
        }
      }
    })
  }

  subscribeStockData() {
 
    this._priceService.getPriceHistory(this.stockList[this.current].name)
      .subscribe(res => {
        this.price_date = res['dataset_data'].data.map(getPriceDates)
        this.stock_price = res['dataset_data'].data.map(getClosePrices)

        function getPriceDates(item, index) { return item[0]; }
        function getClosePrices(item, index) { return item[4]; }

        if (!this.chart.data) { this.createChart();}
         
        this.chart.options.title.text = "Stock Price Chart for " + this.stockList[this.current].name;

        this.chart.data.datasets.pop();

        this.chart.data.datasets.push({
          labels: this.price_date,
          label: "Daily Close",
          fillColor: "#fff",
          fill: true,
          data: this.stock_price
        });
        this.chart.update();



      })  // End of subscribe
  }



}
