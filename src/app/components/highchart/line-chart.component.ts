import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { PriceService } from '../../services/price.service';
import { chart } from 'highcharts';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-highchart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})

export class HighChartComponent implements OnInit {

  title: string = 'Line Chart using HighChart';  

  @ViewChild('chartTarget') chartTarget: ElementRef;

  chart: Highcharts.ChartObject;
 
  stockList: any = [
    { id: 0, name: 'IBM' },
    { id: 1, name: 'FB' },
    { id: 2, name: 'MSFT' },
    { id: 3, name: 'TSLA' },
    { id: 4, name: 'C' },
    { id: 5, name: 'JPM' },
    { id: 6, name: 'AMZN' },
    { id: 7, name: 'GS' }
  ];

  current = 2;
   

  constructor(private _priceService: PriceService ) {  
  }

  ngOnInit(){
    
    this.subscribeStockData(this.stockList[this.current].name); 
 
  }
   
  
  
  logDropdown(id: number): void {
    this.subscribeStockData(this.stockList[this.current].name); 

  }

  subscribeStockData(sym:string) { 

    this._priceService.getPriceHistory(sym)
      .subscribe(res => { 
 
        var price_date = res['dataset_data'].data.map(getPriceDates)
        var stock_price = res['dataset_data'].data.map(getClosePrices) 

        function getPriceDates(item, index) { return item[0]; }
        function getClosePrices(item, index) { return +item[4]; }

        const options : Highcharts.Options = {
          chart: {
            type: 'line'
          },
           
          title: {
            text: 'Stock Price Chart'
          },

          xAxis: {
            categories: price_date
          },
           
          yAxis: {
            title: {
              text: 'Closing Price'
            }
          }, 

          series: [{
            name: sym,
            data: stock_price
          }
         ]
        };

        this.chart = chart(this.chartTarget.nativeElement, options); 

       })   
        
  }
 
}
