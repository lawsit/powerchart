import { Component, OnInit, ViewEncapsulation , Input, Output, EventEmitter  } from '@angular/core';
declare let d3: any;

@Component({
   selector: 'app-donut-chart',
  templateUrl: './donut-chart.component.html',
  styleUrls: ['./donut-chart.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class DonutChartComponent implements OnInit {
    tech_title: string = 'Donut Chart using Angular-nvD3'; 
    title: string = 'Invesment Portfolio by sector';
 

  options;
  data;
  ngOnInit() {
    this.options = {
            chart: {
                type: 'pieChart',
                height: 700,
                donut: true,
                x: function(d){return d.key;},
                y: function(d){return d.y;},
                color : ['#3eb049',  '#649AF6', '#9A97F0', '#C4689E' , '#F7464A', '#97D672'  ],
                showLabels: false,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                legend: {
                    margin: {
                        top: 5,
                        right: 35,
                        bottom: 5,
                        left: 0
                    }
                },
 
 
                pie: {
                dispatch: { 
                    elementMouseover: function(e) { 
                        var svg = d3.select("#DonutChart svg");
                        var donut = svg.selectAll("g.nv-pie").filter(
                             function (d, i) {
                                return i == 1;
                             });
                         
                        svg.selectAll("#ctr-text").remove();

                        donut.insert("text", "g") 
                             .text(  e.data.key )  
                             .attr("class", "css-label-class")
                             .attr("id", "ctr-text")
                             .attr("text-anchor", "middle");
                      
                    },
                    elementMouseout: function(e) {
                        var svg = d3.select("#DonutChart svg");
                        var donut = svg.selectAll("g.nv-pie").filter(
                             function (d, i) {
                                return i == 1;
                             });
                         
                        svg.selectAll("#ctr-text").remove();

                        donut.insert("text", "g")
                             .text("Investment Portfolio")
                             .attr("class", "css-label-class")
                             .attr("id", "ctr-text")
                             .attr("text-anchor", "middle");


                    }  
                }
              }
            }
        };

        

        this.data =  [
            {
                key: "Utilities",
                y: 25000
            },
            {
                key: "Oil & Gas",
                y: 10000
            },
            {
                key: "Public Sector",
                y: 5000
            },
            {
                key: "Transportation",
                y: 70000
            },
            {
                key: "Telcom",
                y: 90000
            } 
        ];

        
  }





}
